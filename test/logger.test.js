const { Logger } = require('../app/lib/logger');
const winston = require('winston');
const os = require('os');

jest.mock('winston', () => ({
  createLogger: jest.fn(() => ({
    log: jest.fn(),
  })),
  format: {
    combine: jest.fn(),
    colorize: jest.fn(),
    timestamp: jest.fn(),
    printf: jest.fn(),
    json: jest.fn(),
  },
  transports: {
    Console: jest.fn(),
  },
}));

describe('Logger', () => {
  const packageJson = { name: 'test-app', version: '1.0.0' };

  it('should throw an error if pkg is not an object', () => {
    expect(() => Logger(null)).toThrow('package,json must be an object');
  });

  it('should throw an error if pkg does not contain name and version fields', () => {
    expect(() => Logger({ name: 'test-app' })).toThrow('package,json must contain name and version fields');
    expect(() => Logger({ version: '1.0.0' })).toThrow('package,json must contain name and version fields');
  });

  it('should log error messages correctly', () => {
    const logger = Logger(packageJson);
    const message = 'Error message';
    const eventTypes = ['event1', 'event2'];

    logger.error(message, eventTypes);
    const winstonInstance = winston.createLogger.mock.results[0].value;

    expect(winstonInstance.log).toHaveBeenCalledWith({
      message,
      app_name: packageJson.name,
      app_version: packageJson.version,
      event_types: eventTypes,
      host_name: os.hostname(),
      level: 'error',
      ts: expect.any(String),
    });
  });

  it('should log warn messages correctly', () => {
    const logger = Logger(packageJson);
    const message = 'Warning message';
    const eventTypes = ['event1', 'event2'];

    logger.warn(message, eventTypes);
    const winstonInstance = winston.createLogger.mock.results[0].value;

    expect(winstonInstance.log).toHaveBeenCalledWith({
      message,
      app_name: packageJson.name,
      app_version: packageJson.version,
      event_types: eventTypes,
      host_name: os.hostname(),
      level: 'warn',
      ts: expect.any(String),
    });
  });

  it('should log debug messages correctly', () => {
    const logger = Logger(packageJson);
    const message = 'Debug message';
    const eventTypes = ['event1', 'event2'];

    logger.debug(message, eventTypes);
    const winstonInstance = winston.createLogger.mock.results[0].value;

    expect(winstonInstance.log).toHaveBeenCalledWith({
      message,
      app_name: packageJson.name,
      app_version: packageJson.version,
      event_types: eventTypes,
      host_name: os.hostname(),
      level: 'debug',
      ts: expect.any(String),
    });
  });
});