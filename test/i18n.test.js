const fs = require('fs');
const path = require('path');
const { logger } = require('../app/lib/logger');
const i18nModule = require('../app/middleware/i18n');

jest.mock('fs');
jest.mock('path');
jest.mock('i18next');
jest.mock('i18next-http-middleware');
jest.mock('i18next-fs-backend');
jest.mock('../app/lib/logger.js');

describe('i18n initialization', () => {
  let app;

  beforeEach(() => {
    app = {
      use: jest.fn(),
    };

    fs.readdirSync.mockReturnValue(['common.json', 'home.json']);
    path.resolve.mockImplementation((...args) => args.join('/'));
    path.parse.mockImplementation((filePath) => ({
      name: filePath.split('/').pop().split('.')[0],
    }));
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should catch and log error during setup', () => {
    const error = new Error('Setup error');
    fs.readdirSync.mockImplementation(() => {
      throw error;
    });

    expect(() => i18nModule(app)).toThrow(error);
    expect(logger.error).toHaveBeenCalledWith('Error initializing i18n', error);
  });
});