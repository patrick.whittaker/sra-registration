const fs = require('fs');
const path = require('path');
const i18next = require('i18next');
const i18nMiddleware = require('i18next-http-middleware');
const FSBackend = require('i18next-fs-backend');
const { logger } = require('../lib/logger.js');

module.exports = (app) => {
  try {
    // Load the list of namespaces (i.e., translation files) available in the 'en' locale
    const namespaces = fs.readdirSync(path.resolve(__dirname, '../locales/en')).map((file) => path.parse(file).name);

    i18next
      .use(FSBackend)
      .use(i18nMiddleware.LanguageDetector)
      .init({
        debug: false,
        ns: namespaces,
        backend: {
          // Path to load translation files from
          loadPath: path.join(__dirname, '/../locales/{{lng}}/{{ns}}.json'),
        },
        initImmediate: false, // Set to false to ensure i18n is initialized before serving requests
        detection: {
          // Order and from where user language should be detected
          order: ['querystring', 'cookie', 'header'],

          // Keys or params to lookup language from
          lookupQuerystring: 'lng',
          lookupCookie: 'lng',
          lookupHeader: 'accept-language',
          lookupSession: 'lng',

          // Domain for the language cookie
          cookieDomain: process.env.LANG_COOKIE_DOMAIN || undefined,

          // Cache user language in cookies
          caches: ['cookie'],
        },
        preload: ['en', 'cy'], // Preload 'en' and 'cy' locales
        fallbackLng: 'en', // Fallback language if user language not found
      }, (err) => {
        if (err) {
          // Log the error if i18n initialization fails
          logger.error('Error loading i18n content', err, null, { stack_function: 'i18n' });
          throw err;
        }
      });

    // Apply i18next middleware to the app
    app.use(
      i18nMiddleware.handle(i18next, {
        ignoreRoutes: ['/public'], // Ignore i18n for public routes
      }),
    );
  } catch (err) {
    // Catch any error that occurs during initialization and log it
    logger.error('Error initializing i18n', err);
    throw err;
  }
};