const { createLogger, format, transports } = require('winston');
const os = require('os');
const packageJson = require('../../package.json');

const logFormat = process.env.LOG_FORMAT || 'JSON';

const loggerFormat = () => {
  if (logFormat === 'SIMPLE') {
    return format.combine(
      format.colorize(),
      format.timestamp({
        format: 'DD-MM-YYYY HH:mm:ss',
      }),
      format.printf((info) => `${info.timestamp} ${info.level} ${info.message} ${info.event_types ? JSON.stringify(info.event_types) : ''}`),
    );
  }

  return format.json();
};

const winstonLogger = createLogger({
  level: process.env.LOG_LEVEL || 'info',
  format: loggerFormat(),
  transports: [new transports.Console()],
});

const Logger = (pkg, options = {}) => {
  if (Object.prototype.toString.call(pkg) !== '[object Object]') {
    throw new TypeError('package,json must be an object');
  }
  if (!Object.prototype.hasOwnProperty.call(pkg, 'name') || !Object.prototype.hasOwnProperty.call(pkg, 'version')) {
    throw new TypeError('package,json must contain name and version fields');
  }

  const appName = pkg.name;
  const appVersion = pkg.version;
  const hostName = os.hostname();
  const { flattenMessage, ...defaults } = options;

  const getSimpleMessage = (message) => {
    if (typeof message === 'object') {
      return JSON.stringify(message);
    }
    return message;
  };

  const getFlattenedMessage = (message) => {
    if (flattenMessage && typeof message === 'object' && !Array.isArray(message)) {
      return message;
    }
    return { message };
  };

  const log = (level, message, eventTypes) => {
    if (logFormat === 'SIMPLE') {
      winstonLogger.log({
        level,
        message: getSimpleMessage(message),
        event_types: eventTypes,
        ts: (new Date(Date.now())).toISOString(),
      });
      return;
    }

    winstonLogger.log({
      message,
      ...getFlattenedMessage(message),
      ...defaults,
      app_name: appName,
      app_version: appVersion,
      event_types: eventTypes,
      host_name: hostName,
      level,
      ts: (new Date(Date.now())).toISOString(),
    });
  };

  return {
    options,
    error: (message, eventTypes) => log('error', message, eventTypes),
    warn: (message, eventTypes) => log('warn', message, eventTypes),
    info: (message, eventTypes) => log('info', message, eventTypes),
    debug: (message, eventTypes) => log('debug', message, eventTypes),
  };
};

module.exports = {
  Logger,
  logger: Logger({ name: 'kbv', version: packageJson.version }, { flattenMessage: true }),
};