$(document).ready(() => {
    $('#btn-clear').on('click', () => {
      window.location.href = '/';
    });
  });
  
  // For date input - set default, minimum & maximum
  const dateInput = $('#clientSecretExpiryTime');
  const today = new Date();
  const oneYearFromNow = new Date(today);
  oneYearFromNow.setFullYear(today.getFullYear() + 1);
  const twoYearsFromNow = new Date(today);
  twoYearsFromNow.setFullYear(today.getFullYear() + 2);
  dateInput.val(oneYearFromNow.toISOString().split('T')[0]);
  dateInput.attr('min', today.toISOString().split('T')[0]);
  dateInput.attr('max', twoYearsFromNow.toISOString().split('T')[0]);
  
  const toggleAccordion = (header) => {
    const content = header.nextElementSibling
    if (content.style.display === "block") {
      content.style.display = "none"
      header.textContent = "Tell me more"
    } else {
      content.style.display = "block"
      header.textContent = "Hide"
    }
  }