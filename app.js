const express= require('express');
const nunjucks = require('nunjucks');
const path = require('path');
const app = express();
const i18nMiddleware = require('./app/middleware/i18n');

nunjucks.configure([
  path.resolve(__dirname, 'app/views'),
  path.resolve(__dirname, 'node_modules/govuk-frontend/dist/govuk/'),
  path.resolve(__dirname, 'node_modules/govuk-frontend/dist/govuk/components'),
], {
  autoescape: true,
  express: app,
});

i18nMiddleware(app);
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

const PORT = 3000;

app.use(express.static(`${__dirname}/`));

app.get('/', (req, res) => {
    res.render('register.njk');
});

app.listen(PORT, err => {
    if (err){
        console.error('Error starting server', err);
        process.exit(1);
    };
    console.log(`Up and running on port ${PORT}`);
});